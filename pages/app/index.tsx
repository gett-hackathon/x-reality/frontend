import './index.scss';

import { ImmerComponent } from '~/components/_base';
import { NextContext } from 'next';
import { Fragment } from 'react';
import Carousel from 'nuka-carousel';
import { Layout } from '~/components/layout';
import Head from 'next/head';

const oculusLogo = require('~/images/vendors/oculus.png');
const msLogo = require('~/images/vendors/ms.png');
const viveLogo = require('~/images/vendors/vive.png');
const googleLogo = require('~/images/vendors/google.png');
const arrowSvg = require('~/images/arrow.svg');

type AppProps = {
    initalApp: {
        logo: string;
        title: string;
        creator: string;
        description: string;
        category: string;
        downloads: {
            vive: string | null;
            oculus: string | null;
            google: string | null;
            micorsoft: string | null;
        };
    };
};
export default class App extends ImmerComponent<AppProps, { popup: boolean }> {
    static async getInitialProps(ctx: NextContext): Promise<AppProps> {
        return {
            initalApp: {
                title: 'Barbra Collector XR',
                creator: 'barbra.io',
                logo:
                    'https://assets.gitlab-static.net/uploads/-/system/project/avatar/9665600/Logo__2_.png?width=22',
                category: 'Marketing',
                downloads: {
                    google: 'https://google.de',
                    micorsoft: 'null',
                    oculus: 'null',
                    vive: 'null'
                },
                description:
                    'NEVER LOSE SIGHT OF ANYTHING YOU LOVE. Use the Barbra Collector to save things you see on the web into your Barbra collections. No more bookmarks or notebooks required. With collections you can simultaneously save content and organise it. This way you can share it with many others. Save the web pages you want to keep. Add them to a Barbra Collection. Easily find them on barbra.io. COLLECT IT ALL - Great for research — collect any article or web page - Great for sharing knowledge among a team - Add to a specific collection and sort it in a section - Then use Barbra to share that collection with people who might find it useful'
            }
        };
    }

    constructor(props: any) {
        super(props);

        this.state = {
            popup: false
        };
    }

    render() {
        return (
            <Layout>
                <Head>
                    <title>
                        {this.props.initalApp.title + ' | XReality Store'}
                    </title>
                </Head>
                {this.state.popup ? (
                    <div
                        className={'popup'}
                        onClick={() => {
                            this.mutateState(state => {
                                state.popup = false;
                            });
                        }}
                    >
                        <div
                            className={'popup-container --download'}
                            onClick={ev => ev.stopPropagation()}
                        >
                            <div className={'title'}>
                                {'Select Your Device'}
                            </div>
                            <div>
                                {this.props.initalApp.downloads.micorsoft !==
                                null ? (
                                    <div>
                                        <img src={msLogo} />
                                        <button
                                            onClick={() => {
                                                window.open(
                                                    this.props.initalApp
                                                        .downloads.micorsoft!
                                                );
                                                this.mutateState(state => {
                                                    state.popup = false;
                                                });
                                            }}
                                        >
                                            {'Download App for HoloLens'}
                                        </button>
                                    </div>
                                ) : null}
                                {this.props.initalApp.downloads.vive !==
                                null ? (
                                    <div>
                                        <img src={viveLogo} />
                                        <button
                                            onClick={() => {
                                                window.open(
                                                    this.props.initalApp
                                                        .downloads.vive!
                                                );
                                                this.mutateState(state => {
                                                    state.popup = false;
                                                });
                                            }}
                                        >
                                            {'Download App for Vive'}
                                        </button>
                                    </div>
                                ) : null}
                                {this.props.initalApp.downloads.oculus !==
                                null ? (
                                    <div>
                                        <img src={oculusLogo} />
                                        <button
                                            onClick={() => {
                                                window.open(
                                                    this.props.initalApp
                                                        .downloads.oculus!
                                                );
                                                this.mutateState(state => {
                                                    state.popup = false;
                                                });
                                            }}
                                        >
                                            {'Download App for Oculus'}
                                        </button>
                                    </div>
                                ) : null}
                                {this.props.initalApp.downloads.google !==
                                null ? (
                                    <div>
                                        <img src={googleLogo} />
                                        <button
                                            onClick={() => {
                                                window.open(
                                                    this.props.initalApp
                                                        .downloads.google!
                                                );
                                                this.mutateState(state => {
                                                    state.popup = false;
                                                });
                                            }}
                                        >
                                            {'Download App for Cardboard'}
                                        </button>
                                    </div>
                                ) : null}
                            </div>
                        </div>
                    </div>
                ) : null}
                <div className={'app-hero'}>
                    <div className={'app-hero__logo'}>
                        <img src={this.props.initalApp.logo} />
                    </div>

                    <div className={'app-hero__info'}>
                        <span className={'app-hero__info__title'}>
                            {this.props.initalApp.title}
                        </span>
                        <span className={'app-hero__info__by'}>
                            {'Offered by: '}
                            <span className={'app-hero__info__by__name'}>
                                {this.props.initalApp.creator}
                            </span>
                        </span>
                        <span className={'app-hero__info__by__desc'}>
                            {this.props.initalApp.category}
                        </span>
                    </div>
                    <div className={'app-hero__download'}>
                        <button
                            className={'app-hero__download__button'}
                            onClick={() => {
                                this.mutateState(state => {
                                    state.popup = true;
                                });
                            }}
                        >
                            {'Download'}
                        </button>
                        <div className={'app-hero__download__available'}>
                            <span>{'Available for'}</span>
                            <div className={'app-download__available__vendors'}>
                                {this.props.initalApp.downloads.micorsoft !==
                                null ? (
                                    <img src={msLogo} />
                                ) : null}
                                {this.props.initalApp.downloads.vive !==
                                null ? (
                                    <img src={viveLogo} />
                                ) : null}
                                {this.props.initalApp.downloads.oculus !==
                                null ? (
                                    <img src={oculusLogo} />
                                ) : null}
                                {this.props.initalApp.downloads.google !==
                                null ? (
                                    <img src={googleLogo} />
                                ) : null}
                            </div>
                        </div>
                    </div>
                </div>

                <div className={'app-preview'}>
                    <Carousel
                        slideIndex={0}
                        width={'700px'}
                        renderCenterLeftControls={({ previousSlide }) => (
                            <button
                                className={'slider__prev'}
                                onClick={previousSlide}
                            >
                                <img src={arrowSvg} />
                            </button>
                        )}
                        renderCenterRightControls={({ nextSlide }) => (
                            <button
                                className={'slider__next'}
                                onClick={nextSlide}
                            >
                                <img src={arrowSvg} />
                            </button>
                        )}
                        afterSlide={currentSlide => {
                            const list = window.document.querySelector(
                                '.slider-list'
                            );
                            if (list) {
                                const slide = list.childNodes[currentSlide];
                                (slide as HTMLLIElement).style.height = '500px';
                            }
                        }}
                        autoplayInterval={4000}
                        wrapAround
                        autoplay
                        pauseOnHover
                        heightMode={'first'}
                    >
                        <img
                            src={
                                'https://cdn.lynda.com/course/587658/587658-636415060813425787-16x9.jpg'
                            }
                        />
                        <img src="https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RE1GJzk?ver=8b59&q=60&m=6&h=600&w=2048&b=%23FFFFFFFF&l=f&o=t&aim=true" />
                    </Carousel>
                </div>

                <div className={'app-info'}>
                    <div className={'app-info__desc'}>
                        <div className={'app-info__desc__title'}>
                            {'Description'}
                        </div>
                        {this.props.initalApp.description}
                    </div>
                </div>
                <div className={'app-footer'}>
                    <div className={'app-footer__title'}>
                        {'Download ' + this.props.initalApp.title}
                    </div>
                    <button
                        onClick={() => {
                            this.mutateState(state => {
                                state.popup = true;
                            });
                        }}
                    >
                        {'Download'}
                    </button>
                    <div className={'app-footer__available'}>
                        <span>{'Available for'}</span>
                        <div className={'app-footer__available__vendors'}>
                            {this.props.initalApp.downloads.micorsoft !==
                            null ? (
                                <img src={msLogo} />
                            ) : null}
                            {this.props.initalApp.downloads.vive !== null ? (
                                <img src={viveLogo} />
                            ) : null}
                            {this.props.initalApp.downloads.oculus !== null ? (
                                <img src={oculusLogo} />
                            ) : null}
                            {this.props.initalApp.downloads.google !== null ? (
                                <img src={googleLogo} />
                            ) : null}
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}
