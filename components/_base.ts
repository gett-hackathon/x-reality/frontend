import { PureComponent } from 'react';
import { produce, Draft } from 'immer';

export class ImmerComponent<
    TProps extends {} = {},
    TStates extends {} = {}
> extends PureComponent<TProps, TStates> {
    public mutateState(mutater: (state: Draft<TStates>) => TStates | void) {
        const newState = produce<TStates>(this.state, mutater);

        super.setState(newState);
    }

    public setState() {
        console.warn('Please use mutateState!');
    }
}

class Test extends ImmerComponent<{}, { test: String }> {
    constructor() {
        super({});

        this.state = {
            test: 'test'
        };
    }

    test() {
        this.mutateState(state => {
            state.test = 'test';
        });
    }
}
