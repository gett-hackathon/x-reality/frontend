import './index.scss';

import { ImmerComponent } from '~/components/_base';
import { Fragment } from 'react';
import Particles from 'react-particles-js';
import { Layout } from '~/components/layout';
import Head from 'next/head';

export default class App extends ImmerComponent {
    render() {
        return (
            <Fragment>
                <Head>
                    <title>{'XReality Store'}</title>
                </Head>
                <div className={'home'}>
                    <Particles
                        className={'home-anim'}
                        height={'100%'}
                        width={'100%'}
                        params={{
                            particles: {
                                number: {
                                    value: 70
                                },
                                size: {
                                    value: 3
                                },
                                color: {
                                    value: '#0D74FF'
                                },
                                line_linked: {
                                    color: '#0D74FF'
                                }
                            },
                            interactivity: {
                                events: {
                                    onhover: {
                                        enable: true,
                                        mode: 'repulse'
                                    }
                                }
                            }
                        }}
                    />
                    <div className={'home-info'}>
                        <img src={'/static/images/logo.svg'} />
                        <div className={'home-info__title'}>
                            <span>{'XReality '}</span>
                            <span>{'Store'}</span>
                        </div>
                        <div className={'home-info__desc'}>
                            {'A new way to find VR applications'}
                        </div>

                        <button>{'Login'}</button>
                    </div>
                </div>
            </Fragment>
        );
    }
}
