export const graphqlEndpoint = (() => {
    if (process.env.NODE_ENV === 'development') {
        return 'http://localhost:7070/graphql';
    } else {
        return 'https://api.barbra.io/graphql';
    }
})();

export const metadataEndpoint = (() => {
    if (process.env.NODE_ENV === 'development') {
        return 'http://localhost:7070/metadata';
    } else {
        return 'https://barbra.io/metadata';
    }
})();

export const extauth = 'https://api.barbra.io/extauth';
