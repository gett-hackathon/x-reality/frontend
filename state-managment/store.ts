import { init, RematchRootState } from '@rematch/core';
import { models } from './models';

export const store = init({
    models,
    redux: {
        devtoolOptions: {
            disabled: false
        }
    }
});

export type Dispatch = typeof store.dispatch;
export type State = RematchRootState<typeof models>;
