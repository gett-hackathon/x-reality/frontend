const withTypescript = require('@zeit/next-typescript');
const withSass = require('@zeit/next-sass');

const config = withSass({
    sassLoaderOptions: {
        includePaths: ['./styles']
    },

    ...withTypescript({
        webpack(config) {
            // Fixes npm packages that depend on `fs` module
            config.node = {
                fs: 'empty'
            };

            config.resolve.extensions.push('.png');
            config.resolve.extensions.push('.svg');
            config.resolve.extensions.push('.woff');
            config.resolve.extensions.push('.woff2');
            config.resolve.extensions.push('.ttf');

            config.module.rules.push({
                test: /\.(woff(2)?|ttf|png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: `static/[path]${
                                process.env.NODE_ENV === 'development'
                                    ? '[name].'
                                    : ''
                            }[hash].[ext]`,

                            publicPath: '/_next/'
                        }
                    },
                    {
                        loader: 'image-webpack-loader'
                    }
                ]
            });

            return config;
        }
    })
});

module.exports = config;
